// #8b729c
function ChangeColor(content) {
    let mCard = document.querySelector('.message-card');
    mCard.style.background = content.value;
    document.querySelector('#cardColor2').value = content.value;
}
function ChangeColor2(content) {
    let mCard = document.querySelector('.message-card');
    mCard.style.background = content.value;
    document.querySelector('#cardColor').value = content.value;
}
// #b4abba
function ChangeFColor(content) {
    let mFont = document.querySelector('.message-text-foot');
    mFont.style.color = content.value;
    document.querySelector('#footFColor2').value = content.value;
}

function ChangeFColor2(content) {
    let mFont = document.querySelector('.message-text-foot');
    mFont.style.color = content.value;
    document.querySelector('#footFColor').value = content.value;
}

function ChangeImg(content) {
    let file = content.files[0];
    if (file) {
        let reader = new FileReader();
        reader.onload = (event) => {
            document.querySelector('#self-img').src = event.target.result;
        }
        reader.readAsDataURL(file);
    }
}

function ChangeFontContent(content) {
    let mFontContent = document.querySelector('.message-text-foot');
    mFontContent.textContent = content.value;
}

function ChangeUrlApi(content) {
    let url = content.value;
    if (url != null && url != "") {
        var xhr = new XMLHttpRequest();
        let jsonRes = [];
        xhr.onload = function () {
            if (xhr.status == 200) {
                jsonRes = JSON.parse(xhr.responseText);
                delete jsonRes[0].index;
                let column = Object.keys(jsonRes[0]);
                document.querySelector('#DContainer').innerHTML = '';
                column.forEach(element => {
                    let newliElement = document.createElement('li');
                    newliElement.classList.add('draggable');
                    newliElement.setAttribute('draggable', true);

                    let newDelBtnElement = document.createElement('button');
                    newDelBtnElement.classList.add('btn-delete');

                    newDelBtnElement.append('X');
                    newliElement.append(element);
                    newliElement.append(newDelBtnElement);
                    document.querySelector('#DContainer').appendChild(newliElement);
                    newDelBtnElement.addEventListener('click', function () {
                        newliElement.remove();
                    });
                });
                let items = document.querySelectorAll('#DContainer > li');
                items.forEach(item => {
                    item.addEventListener('dragstart', dragStart)
                    item.addEventListener('drop', dropped)
                    item.addEventListener('dragenter', cancelDefault)
                    item.addEventListener('dragover', cancelDefault)
                });
            } else {
                console.log('error');
            }
        }
        xhr.open('get', url)
        xhr.send();
    }
}

function dragStart(e) {
    var index = Array.from(e.target.parentNode.children).indexOf(e.target);
    e.dataTransfer.setData('text/plain', index);
}

function dropped(e) {
    cancelDefault(e);

    // 获取新旧索引
    var oldIndex = e.dataTransfer.getData('text/plain');
    var target = e.target;
    var newIndex = Array.from(target.parentNode.children).indexOf(target);

    // 从旧位置移除拖动的元素
    var parent = target.parentNode;
    var dropped = parent.children[oldIndex];
    parent.removeChild(dropped);

    // 插入拖动的元素到新位置
    if (newIndex < oldIndex) {
        parent.insertBefore(dropped, target);
    } else {
        parent.insertBefore(dropped, target.nextSibling);
    }
}

function cancelDefault(e) {
    e.preventDefault();
    e.stopPropagation();
    return false;
}


function DownLoadSelfHtml() {

    let cardColor = document.querySelector('#cardColor').value;
    let selfImg = document.querySelector('#self-img').src;
    let footFColor = document.querySelector('#footFColor').value;
    let footFontContent = document.querySelector('#footFontContent').value;
    let apiURL = document.querySelector('#apiURL').value;

    if (apiURL != "" && apiURL != null) {
        let downloadHtml =
            `
<html>
<meta charset="utf-8">

<head>
    <style>

    </style>
    <script>
        var xhr = new XMLHttpRequest();
        let jsonRes = [];
        xhr.onload = function () {
            if (xhr.status == 200) {
                jsonRes = JSON.parse(xhr.responseText);
                let currData = jsonRes[localStorage.currNumber == null ?0 : (localStorage.currNumber-1)];
                document.querySelector('.message-text').textContent = "";

                ${CreateElement()}
                
                document.querySelector('#form-count').textContent = jsonRes.length;
                document.querySelector('.change-number').value = localStorage.currNumber == null ? 1: localStorage.currNumber;
            } else {
                console.log('error');
            }
        }
        xhr.open('get', '${apiURL}')
        xhr.send();
        function addNumber(){
            var currNumber = document.querySelector('.change-number').value;
            if(currNumber != jsonRes.length){
                currNumber ++;
                document.querySelector('.change-number').value = currNumber;
                document.querySelector('.message-text').innerHTML = "";
                let currData = jsonRes[currNumber-1];
                ${CreateElement()}
                localStorage.setItem('currNumber' , currNumber);
            }
        }
        function reNumber(){
            var currNumber = document.querySelector('.change-number').value;
            if(currNumber != 0){
                currNumber --;
                document.querySelector('.change-number').value = currNumber;
                document.querySelector('.message-text').innerHTML = "";
                let currData = jsonRes[currNumber-1];
                ${CreateElement()}
                localStorage.setItem('currNumber' , currNumber);
            }
        }
        function inputNumber(){
            var currNumber = document.querySelector('.change-number').value;
            document.querySelector('.message-text').textContent = jsonRes[currNumber-1].content;
            localStorage.setItem('currNumber' , currNumber);
        }
    </script>
    <style>
        * {
            font-family: system-ui;
        }

        .message-group {
            display: flex;
            flex-direction: column;
            align-items: center;
            margin-top: 40px;
        }

        .message-card {
            width: 600px;
            padding: 10px;
            background: ${cardColor};
            border-radius: 8px;
        }

        .message-body {
            width: 600px;
            background: white;
            border-radius: 8px;
            position: relative;
        }

        .message-text {
            width: 555px;
            min-height: 300px;
            font-size: 24px;
            word-break: break-all;
            position: relative;
            padding: 20px;
            line-height: 1.5;
        }

        .message-foot {
            display: flex;
            justify-content: center;
            margin-top: 5px;
        }

        .message-text-foot {
            font-family: "Brush Script MT", "rounded mplus 1c", "m plus rounded 1c", sans-serif;
            padding: 0.5em 0;
            display: flex;
            color: ${footFColor};
            justify-content: center;
            align-items: center;
            font-size: 1.25rem;
        }

        .btn-left {
            background-image: url(https://gitlab.com/thesixleafs/seflmarshmallow/-/raw/main/public/img/left.svg);
        }

        .btn-right {
            background-image: url(https://gitlab.com/thesixleafs/seflmarshmallow/-/raw/main/public/img/right.svg);
        }

        .change-btn {
            display: inline-block;
            width: 2rem;
            height: 1.25rem;
            background-repeat: no-repeat;
            background-position: 50%;
            background-size: 100% 100%;

        }

        .button {
            color: #fff;
            background-color: #6c757d;
            border-color: #6c757d;
            border: 1px solid transparent;
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            border-radius: 0.25rem;
        }

        .change-group {
            display: flex;
            justify-content: center;
            align-items: center;
            width: 600px;
            font-size: 24px;
            margin-bottom: 15px;
        }

        .change-number {
            font-size: 24px;
            width: 100px;
            text-align: right;
            border-radius: 0.25rem;
            background-clip: padding-box;
            border: 1px solid #ced4da !important;
        }

        .change-number:hover,
        .change-number:focus {
            color: #212529;
            background-color: #fff;
            border-color: #86b7fe;
            outline: 0;
            box-shadow: 0 0 0 0.25rem rgba(13, 110, 253, .25);
        }

        .button:hover {
            color: #fff;
            background-color: #5c636a;
            border-color: #565e64;
        }
    </style>
</head>

<body>
    <div class="message-group">
        <div class="change-group">
            <button class="button" onclick="reNumber()">
                <span class="change-btn btn-left"></span>
            </button>
            <input type="number" oninput="inputNumber()" class="change-number">
            <span style="background: white;">共</span>
            <span style="background: white;" id="form-count">100</span>
            <span style="background: white;">則</span>
            <button class="button" onclick="addNumber()">
                <span class="change-btn btn-right"></span>
            </button>
        </div>
        <div class="message-card">
            <div class="message-body">
                <div class="message-text">這邊會是表單內容的文字喔，然後字數太多會換行，目前在正製作樣式而已，還沒有資料進來唷。</div>
                <div class="message-text-foot">${footFontContent}</div>
            </div>
            <div class="message-foot">
                <img src="${selfImg}" height="70">
            </div>
        </div>
    </div>

</body>

</html>
            `
        // 将HTML内容转换为Blob对象
        var blob = new Blob([downloadHtml], { type: 'text/html' });

        // 创建一个包含Blob的URL
        var url = URL.createObjectURL(blob);

        // 创建一个下载链接
        var downloadLink = document.createElement('a');
        downloadLink.href = url;
        downloadLink.download = footFontContent + '.html';
        downloadLink.click();
    }else{
        alert("請輸入資料網址唷OAO/")
    }

}

function CreateElement() {
    let cElement = "";
    document.querySelectorAll('.draggable').forEach(e => {
        let jsonData = e.textContent.replace('X', '');
        let mBottom = "";
        let titleWeight = "";
        if(jsonData.includes('A')){
            mBottom += 
            `
            new${jsonData}.style.marginBottom = 15;
            `
        }else{
            titleWeight +=
            `
            new${jsonData}.style.fontWeight = 900;
            `
        }
        cElement +=
            `
        let new${jsonData} = document.createElement('div');
        ${mBottom}
        ${titleWeight}
        new${jsonData}.innerHTML = (currData.${jsonData} == null ? '':currData.${jsonData});
        document.querySelector('.message-text').appendChild(new${jsonData});
        `;
    });
    return cElement;
}